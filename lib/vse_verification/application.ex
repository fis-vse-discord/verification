defmodule VseVerification.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      VseVerificationWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: VseVerification.PubSub},
      # Start the Endpoint (http/https)
      VseVerificationWeb.Endpoint,
      # Start a worker by calling: VseVerification.Worker.start_link(arg)
      # {VseVerification.Worker, arg}
      VseVerification.Discord.Consumer
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: VseVerification.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    VseVerificationWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
