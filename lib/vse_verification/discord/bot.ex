defmodule VseVerification.Discord.Bot do
  alias Nostrum.Api
  alias Nostrum.Struct.Embed

  @spec validate_guild_membership(integer()) :: :ok | :error
  def validate_guild_membership(user_id) do
    with {:ok, _} <- Api.get_guild_member(guild_id(), user_id) do
      :ok
    else
      _ -> :error
    end
  end

  @spec complete_verification(integer(), String.t(), String.t()) :: :ok | :error
  def complete_verification(discord_id, azure_name, azure_email) do
    with {:ok} <-
           Api.add_guild_member_role(
             guild_id(),
             discord_id,
             role_id(),
             "Verifikace: #{azure_name} (#{azure_email})"
           ) do
      Api.create_message(
        log_channel_id(),
        embeds: [create_log_message_embed(discord_id, azure_name, azure_email)]
      )

      :ok
    else
      _ -> :error
    end
  end

  @spec create_log_message_embed(integer(), String.t(), String.t()) :: Embed.t()
  defp create_log_message_embed(discord_id, azure_name, azure_email) do
    %Embed{}
    |> Embed.put_color(0x57F287)
    |> Embed.put_title("Nová verifikace")
    |> Embed.put_field("Discord", "<@#{discord_id}>")
    |> Embed.put_field("Azure", "**#{azure_name}** (`#{azure_email}`)")
  end

  @spec configuration(atom()) :: any()
  defp configuration(key) do
    {:ok, configuration} = Application.fetch_env(:vse_verification, __MODULE__)
    configuration[key]
  end

  @spec guild_id() :: integer()
  defp guild_id() do
    String.to_integer(configuration(:guild_id))
  end

  @spec role_id() :: integer()
  defp role_id() do
    String.to_integer(configuration(:verification_role_id))
  end

  @spec log_channel_id() :: integer()
  defp log_channel_id() do
    String.to_integer(configuration(:log_channel_id))
  end
end
