defmodule VseVerification.Discord.Consumer do
  alias Nostrum.Struct.Embed
  alias Nostrum.Api

  use Nostrum.Consumer

  require Logger

  def start_link do
    Consumer.start_link(__MODULE__)
  end

  def handle_event({:READY, %{user: user, application: application}, _ws_state}) do
    Logger.info("Logged in as #{user.username} running under application #{application.id}")

    Api.create_global_application_command(application.id, %{
      name: "verification-message",
      description: "Send a verification message in the current channel",
      default_permission: false
    })
  end

  def handle_event({:INTERACTION_CREATE, %{channel_id: channel_id} = interaction, _ws_state}) do
    Api.create_interaction_response(interaction, %{
      type: 4,
      data: %{content: "👍"}
    })

    Api.create_message(channel_id,
      embeds: [verification_embed()],
      components: [verification_button()]
    )
  end

  def handle_event(_event) do
    :noop
  end

  defp verification_embed() do
    configuration = Application.get_env(:vse_verification, VseVerification.Discord.Bot)
    {role_id, _} = Integer.parse(configuration[:verification_role_id])

    %Embed{}
    |> Embed.put_color(0x009EE0)
    |> Embed.put_title("Verifikace školního účtu VŠE pro zpřístupnění serveru")
    |> Embed.put_thumbnail("https://gitlab.com/uploads/-/system/project/avatar/44478326/badge.png")
    |> Embed.put_description("""
      Pro zahájení verifikace klikni na tlačítko pod zprávou.
      Po úspěšné verifikaci ti pak bude přidělena role <@&#{role_id}>, která odemyká přístup na server.

      V případě jakýchkoliv problémů, můžeš napsat komukoliv z adminů (ideálně <@256114627794960384>).
    """)
  end

  defp verification_button() do
    %{
      type: 1,
      components: [
        %{
          type: 2,
          style: 5,
          label: "Verifikace školního účtu",
          url: "https://vse-verification.fly.dev"
        }
      ]
    }
  end
end
