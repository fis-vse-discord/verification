defmodule VseVerificationWeb.PageHTML do
  use VseVerificationWeb, :html

  embed_templates "page_html/*"
end
