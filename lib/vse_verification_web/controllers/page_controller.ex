defmodule VseVerificationWeb.PageController do
  use VseVerificationWeb, :controller

  def home(conn, _params) do
    conn = fetch_session(conn)
    discord_auth = get_session(conn, :discord_auth)
    azure_auth = get_session(conn, :azure_auth)

    conn
    |> assign(:discord_auth, discord_auth)
    |> assign(:azure_auth, azure_auth)
    |> assign(:completed, discord_auth != nil and azure_auth != nil)
    |> render(:home, layout: false)
  end
end
