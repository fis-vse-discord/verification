defmodule VseVerificationWeb.AuthController do
  use VseVerificationWeb, :controller

  alias VseVerification.Discord.Bot

  plug Ueberauth

  # Discord authentication callback
  def callback(
        %{assigns: %{ueberauth_auth: %{uid: id, info: %{nickname: name, image: image}}}} = conn,
        %{"provider" => "discord"}
      ) do
  id = String.to_integer(id)
  if Bot.validate_guild_membership(id) == :ok do
    conn
    |> fetch_session()
    |> put_session(:discord_auth, %{id: id, name: name, image: image})
    |> redirect(to: "/")
  else
    conn
    |> put_flash(:error, "Tvůj účet nebyl na FIS VŠE serveru nalezen.")
    |> redirect(to: "/")
  end
  end

  # Azure authentication callback
  def callback(
        %{
          assigns: %{
            ueberauth_auth: %{
              info: %{name: name, email: email},
              # User needs to be a student within the Azure AD organization
              extra: %{
                raw_info: %{
                  user: %{"jobTitle" => "student"}
                }
              }
            }
          }
        } = conn,
        %{"provider" => "azure"}
      ) do
    with conn <- fetch_session(conn),
         discord_id <- get_session(conn, :discord_auth).id,
         :ok <- Bot.complete_verification(discord_id, name, email) do
      conn
      |> put_session(:azure_auth, %{name: name, email: email})
      |> redirect(to: "/")
    else
      _ ->
        conn
        |> put_flash(:error, "Nepodařilo se ověřit tvůj školní účet VŠE")
        |> redirect(to: "/")
    end
  end

  # Fallback
  def callback(%{} = conn, _params) do
    conn
    |> put_flash(:error, "Nastala chyba při autentikaci")
    |> redirect(to: "/")
  end
end
