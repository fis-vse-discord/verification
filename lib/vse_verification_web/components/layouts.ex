defmodule VseVerificationWeb.Layouts do
  use VseVerificationWeb, :html

  embed_templates "layouts/*"
end
